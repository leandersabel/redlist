# Copyright 2014 Random Robot Softworks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class AccountsTableViewController < UITableViewController

  def viewDidLoad
    @accounts = Array.new

    # Load accounts from the database

    if @accounts.count == 0 
      wizard = CreationWizardFirst.new
      wizard.show
    end
  end


  def tableView(tableView, numberOfRowsInSection:section)
    @accounts ? @accounts.count : 0
  end

  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cellIdentifier = self.class.name
    
    # Get an existing cell or generate a new one
    cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) || begin
      cell = UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:cellIdentifier)
      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator
      cell
    end

    #project = @account.projects[indexPath.row]
    #cell.textLabel.text = project.name
    cell
  end
end
