# Copyright 2014 Random Robot Softworks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class ProjectsTableViewController < UIViewController

  def viewDidLoad
    super

    @table = UITableView.alloc.initWithFrame(self.view.frame)
    self.view.addSubview(@table)
    @table.dataSource = self
    @table.delegate = self

    display_add_account_overlay unless @account


    # Setup the model
    @account = Account.new(
      url: "http://www.redmine.org")
    @account.save!
    #@account.update(lambda { @table.reloadData } )

    accounts = Account.find(:all)
    puts "I have #{accounts.all.size} Accounts"

    self.navigationItem.setTitle("Projects")

    add_button = UIBarButtonItem.alloc.initWithTitle("ADD_ACCOUNT_UIBAR"._, style:UIBarButtonSystemItemAction,  target:self, action:'add_button_tapped:')
    self.navigationItem.setRightBarButtonItem(add_button)
    
  end

  def viewWillAppear(animated)
    remove_add_account_overlay if @account
    super(animated)
  end

  def numberOfSectionsInTableView(tableView)
    1
  end

  def tableView(tableView, numberOfRowsInSection:section)
    @account ? @account.project_ids.count : 0
  end

  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cellIdentifier = self.class.name
    
    # Get an existing cell or generate a new one
    cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) || begin
      cell = UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:cellIdentifier)
      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator
      cell
    end

    project = @account.project_ids[indexPath.row]
    cell.textLabel.text = project.name
    cell
  end

  def tableView(tableView, didSelectRowAtIndexPath:indexPath)
    issues_controller = IssuesTableViewController.alloc.init
    self.navigationController.pushViewController(issues_controller, animated:true)

    project = @account.project_ids[indexPath.row]
    issues_controller.bind_with_project(project)
  end


  def display_add_account_overlay
    #@no_account_info = NSBundle.mainBundle.loadNibNamed("interfaces/no_account_info", owner:self, options:nil)[0]
    #@no_account_info.center = [view.frame.size.width / 2, view.frame.size.height / 3]
    #view.addSubview(@no_account_info)
    
    add_button = UIButton.alloc.init
    add_button.styleClass = 'button add_account_overlay'
    self.view.addSubview(add_button)

    # Configure the custom image button
    #add_button = @no_account_info.viewWithTag(1)
    #add_button.setImage(UIImage.imageNamed("images/150px_add_overlay_normal.svg"), forState:UIControlStateNormal)
    #add_button.setImage(UIImage.imageNamed("images/150px_add_overlay_highlight.svg"), forState:UIControlStateHighlighted)
    #add_button.addTarget(self, action:'add_button_tapped:', forControlEvents:UIControlEventTouchUpInside)
   
    # Add the localized text to the label  
    #info_text = @no_account_info.viewWithTag(2)
    #info_text.setText("ADD_ACCOUNT"._)


  end

  def add_button_tapped(sender)
    @account_creation_wizward = WizardAddAccountController.new
    self.navigationController.pushViewController(@account_creation_wizward, animated: true)
  end


  def remove_add_account_overlay
    #@no_account_info.setHidden(true)
  end

end