# Copyright 2014 Random Robot Softworks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class IssuesTableViewController < UIViewController

  def bind_with_project(project) 
    @project = project
    self.navigationItem.title = project.name
  end
  
  def viewDidLoad
    super
    
    @table = UITableView.alloc.initWithFrame(self.view.frame)
    self.view.addSubview(@table)
    @table.dataSource = self
    @table.delegate = self
    @table.reloadData

  end

  def viewDidUnload
    super

    # Release any retained subviews of the main view.
    # e.g. self.myOutlet = nil
  end

  def tableView(tableView, cellForRowAtIndexPath: indexPath)
    cellIdentifier = self.class.name
    
    # Get an existing cell or generate a new one
    cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) || begin
      cell = UITableViewCell.alloc.initWithStyle(UITableViewCellSelectionStyleBlue, reuseIdentifier:cellIdentifier)
      cell.textLabel.numberOfLines = 0;
      cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping
      cell.textLabel.font = UIFont.systemFontOfSize(16) 
      puts "Cell Width: #{cell.frame.size.width}"     
      cell
    end
    
    # Add data to the cell
    cell.textLabel.text = @project.issues[indexPath.row].subject
    
    cell
  end

  def tableView(tableView, heightForRowAtIndexPath: indexPath)
    cell_content = @project.issues[indexPath.row].subject
    cell_font = UIFont.systemFontOfSize(16)
    label_size = cell_content.sizeWithFont(cell_font, constrainedToSize:[260.0, Float::MAX], lineBreakMode:UILineBreakModeWordWrap)
    label_size.height + 20
  end

  def tableView(tableView, numberOfRowsInSection: section)
    @project.issues.count
  end

end