# Copyright 2014 Random Robot Softworks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class WizardAddAccountController < UIViewController

  def viewDidLoad

    self.view = NSBundle.mainBundle.loadNibNamed("interfaces/account_creation_wizard", owner:self, options:nil)[0]
    self.navigationItem.title = "ACCOUNT_CREATION_WIZARD_TITLE"._
    
    @url_field = view.viewWithTag(1)
    @url_field.delegate = self

    @username_field = view.viewWithTag(2)
    @username_field.delegate = self

    @password_field = view.viewWithTag(3)
    @password_field.delegate = self

    @next_button = view.viewWithTag(4)
    @next_button.setTitle("NEXT_BUTTON_TEXT"._, forState:UIControlStateNormal) 
    @next_button.addTarget(self, action:'next_button_pressed:', forControlEvents:UIControlEventTouchUpInside)
  end


  def textFieldShouldReturn(sender)
    sender.resignFirstResponder
    @username_field.becomeFirstResponder if sender == @url_field
    @password_field.becomeFirstResponder if sender == @username_field
    true
  end

  def next_button_pressed(sender)

    url = @url_field.text
    username = @username_field.text
    password = @password_field.text

    # Validate URL, username and password

    account = Account.new(url, username, password)


    warning = UIAlertView.alloc.initWithTitle("WARNING"._, message: "'#{@url_field.text}'" + "NOT_A_VALID_URL"._, delegate: self, cancelButtonTitle: "Ok" , otherButtonTitles: nil)
    warning.show
  end

end