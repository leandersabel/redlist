# Copyright 2014 Random Robot Softworks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Leander Sabel

class Account
  include MotionModel::Model
  include MotionModel::ArrayModelAdapter

  columns id:           :integer,
          project_name: :string,
          url:          :string, 
          username:     :string,  
          password:     :string, 
          api_key:      :string, 
          project_ids:  {type: :array, default: []}, # use proper relationships
          created_at:   :date,
          updated_at:   :date

  # Load all filelds from the server
  def update(callback = nil)

    # Update Projects
    BW::HTTP.get("#{@url}/projects.json") do |response|  
      json = BW::JSON.parse(response.body.to_str)[:projects]
          
      json.each do |attributes| 
        @projects << Project.new(self, attributes)
      end

      @updated = Time.new
      callback.call if callback
    end
  end
end