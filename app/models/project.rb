# Copyright 2014 Random Robot Softworks
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Leander Sabel

class Project

  PROPERTIES = [:id, :updated_on, :created_on, :name, :description, :identifier]

  attr_accessor :issues, :account

  # Create an attribute accessor for each property
  PROPERTIES.each { |prop|
    attr_accessor prop
  }

  def initialize(account, attributes = {})
    @account = account
    @issues = Array.new

    # Assign each attribute to its local field
    attributes.each  do |key, value|
      if PROPERTIES.member? key.to_sym
        self.send((key.to_s + "=").to_s, value)
      end
    end

    update
  end

  # Update issues
  def update(callback = nil)
    BW::HTTP.get("#{@account.url}/projects/#{@name}/issues.json") do |response|  
      json = BW::JSON.parse(response.body.to_str)[:issues]
      
      json.each do |attributes| 
        @issues << Issue.new(attributes)
      end

      @updated = Time.new
      callback.call if callback
    end
  end
end
